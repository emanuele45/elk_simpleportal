<?php

/**
 * @package SimplePortal
 *
 * @author SimplePortal Team
 * @copyright 2014 SimplePortal Team
 * @license BSD 3-clause
 *
 * @version 2.4
 */

// Configuration area
$txt['sp-adminConfiguration'] = 'Configuration';
$txt['sp-adminConfigurationDesc'] = 'This area allows you to view SimplePortal information and manage SimplePortal settings.';
$txt['sp-adminGeneralSettingsName'] = 'General Settings';
$txt['sp-adminBlockSettingsName'] = 'Block Settings';
$txt['sp-adminArticleSettingsName'] = 'Article Settings';

// General settings
$txt['portalactive'] = 'Enable SimplePortal';
$txt['sp_portal_mode'] = 'Portal Mode';
$txt['sp_portal_mode_options'] = 'Disabled|Front Page|Integration|Standalone';
$txt['sp_maintenance'] = 'Maintenance Mode';
$txt['sp_standalone_url'] = 'Standalone URL';
$txt['sp_disableForumRedirect'] = 'Disable Portal Redirection';
$txt['sp_disableColor'] = 'Disable Colored Member Links';
$txt['sp_disable_random_bullets'] = 'Disable Colored Bullets on Blocks';
$txt['sp_disable_php_validation'] = 'Disable PHP Validation';
$txt['sp_disable_php_validation_desc'] = 'Recommended value: Off';

$txt['sp_disable_side_collapse'] = 'Disable Side Collapsing';
$txt['portaltheme'] = 'Portal Theme';
$txt['portalthemedefault'] = 'Forum Default';
$txt['sp_resize_images'] = 'Resizes images on portal';

// Block settings
$txt['showleft'] = 'Enable Left Side Blocks';
$txt['showright'] = 'Enable Right Side Blocks';
$txt['leftwidth'] = 'Width of Left Side Blocks';
$txt['rightwidth'] = 'Width of Right Side Blocks';
$txt['sp_enableIntegration'] = 'Display Blocks in Forum';
$txt['sp_IntegrationHide'] = 'Hide Blocks in Forum Areas';
$txt['sp_IntegrationHide_desc'] = 'The <em>Display blocks in Forum</em> setting must be enabled for this to work';


// Article settings
$txt['sp_articles_index'] = 'Display Articles on Portal Page';
$txt['sp_articles_index_total'] = 'Total Articles on Portal Page';
$txt['sp_articles_index_per_page'] = 'Articles Per Page on Portal Page';
$txt['sp_articles_per_page'] = 'Articles Per Page on Article List';
$txt['sp_articles_comments_per_page'] = 'Articles Comments Per Page';
$txt['sp_articles_length'] = 'Maximum Characters Before Article Cut-off';

// Blocks area
$txt['sp-adminBlockListName'] = 'Block List';
$txt['sp-adminBlockInuse'] = 'Used in %s Block';
$txt['sp-adminBlockListDesc'] = 'This page displays a list of all blocks which have been created for the portal or the forum. To move a block to a new location you use a simple drag and drop action.';
$txt['sp-adminBlockAddName'] = 'Add Block';
$txt['sp-adminBlockAddDesc'] = 'This page enables new blocks to be created and added to the portal page or the forum.';
$txt['sp-adminBlockLeftListDesc'] = 'This page displays a list of all left side blocks which have been created. These blocks can be modified by selecting the appropriate options.';
$txt['sp-adminBlockRightListDesc'] = 'This page displays a list of all right side blocks which have been created. These blocks can be modified by selecting the appropriate options.';
$txt['sp-adminBlockTopListDesc'] = 'This page displays a list of all top area blocks which have been created. These blocks can be modified by selecting the appropriate options.';
$txt['sp-adminBlockBottomListDesc'] = 'This page displays a list of all bottom area blocks which have been created. These blocks can be modified by selecting the appropriate options.';
$txt['sp-adminBlockHeaderListDesc'] = 'This page displays a list of all header area blocks which have been created. These blocks can be modified by selecting the appropriate options.';
$txt['sp-adminBlockFooterListDesc'] = 'This page displays a list of all footer area blocks which have been created. These blocks can be modified by selecting the appropriate options.';

// Block list
$txt['sp-blocksBlocks'] = 'Blocks';
$txt['sp-blocksActive'] = 'Active';
$txt['sp-blocksActivate'] = 'Activate';
$txt['sp-blocksDeactivate'] = 'Deactivate';
$txt['sp-blocksCreate'] = 'Create %s Block';
$txt['sp-deleteblock'] = 'Are you sure you want to delete this block?';
$txt['sp-blocks_select_destination'] = 'Select destination for block \'<strong>%1$s</strong>\'';
$txt['sp-blocks_move_here'] = 'Move block here';
$txt['sp-blocks_cancel_moving'] = 'Cancel moving';
$txt['sp-blocks_success_moving'] = 'The block was successfully moved';
$txt['sp-blocks_fail_moving'] = 'There was an error trying to move the block';

// Add/Edit blocks
$txt['sp-blocksSelectType'] = 'Select Block Type';
$txt['sp-blocksAdd'] = 'Add Block';
$txt['sp-blocksEdit'] = 'Edit Block';
$txt['sp-blocksPreview'] = 'Preview';
$txt['sp-blocksDefaultLabel'] = 'Untitled';
$txt['sp-blocksDisabledBoth'] = 'Left and right side blocks are currently disabled';
$txt['sp-blocksDisabledLeft'] = 'Left side blocks are currently disabled';
$txt['sp-blocksDisabledRight'] = 'Right side blocks are currently disabled';
$txt['sp-blocksContent'] = 'Content';
$txt['sp-blocksColumn'] = 'Column';
$txt['sp_admin_blocks_col_permissions'] = 'Permissions';
$txt['sp-blocksRow'] = 'Order';
$txt['sp-blocksForce'] = 'Not Collapsible';
$txt['sp-blocksDisplayOptions'] = 'Display Options';
$txt['sp-blocksAdvancedOptions'] = 'Advanced Options';
$txt['sp-blocksShowBlock'] = 'Show Block On';
$txt['sp-blocksOptionAllPages'] = 'All Pages';
$txt['sp-blocksOptionAllActions'] = 'All Actions';
$txt['sp-blocksOptionAllBoards'] = 'All Boards';
$txt['sp-blocksOptionEverywhere'] = 'Everywhere';
$txt['sp-blocksSelectActions'] = 'Select Actions';
$txt['sp-blocksSelectBoards'] = 'Select Boards';
$txt['sp-blocksSelectPages'] = 'Select Pages';
$txt['sp_display_custom'] = 'Custom Display Options';
$txt['sp-blocksStyleOptions'] = 'Style Options';
$txt['sp-blocksTitleDefaultClass'] = 'Default Title Class';
$txt['sp-blocksTitleCustomClass'] = 'Custom Title Class';
$txt['sp-blocksTitleCustomStyle'] = 'Custom Title Style';
$txt['sp-blocksBodyDefaultClass'] = 'Default Body Class';
$txt['sp-blocksBodyCustomClass'] = 'Custom Body Class';
$txt['sp-blocksBodyCustomStyle'] = 'Custom Body Style';
$txt['sp-blocksNoTitle'] = 'No Title';
$txt['sp-blocksNoBody'] = 'No Body';

// Block labels
$txt['sp_function_User_Info_label'] = 'Member Information';
$txt['sp_function_Latest_Member_label'] = 'Latest Members';
$txt['sp_function_Whos_Online_label'] = 'Who\'s Online';
$txt['sp_function_Show_Poll_label'] = 'Poll';
$txt['sp_function_Board_Stats_label'] = 'Board Statistics';
$txt['sp_function_Quick_Search_label'] = 'Quick Search';
$txt['sp_function_Top_Poster_label'] = 'Top Poster';
$txt['sp_function_Top_Stats_Member_label'] = 'Top Member Stats';
$txt['sp_function_Top_Boards_label'] = 'Top Boards';
$txt['sp_function_Top_Topics_label'] = 'Top Topics';
$txt['sp_function_Recent_label'] = 'Recent Posts/Topics';
$txt['sp_function_Board_News_label'] = 'Board News';
$txt['sp_function_News_label'] = 'Forum News';
$txt['sp_function_Attachment_Image_label'] = 'Recent Image Attachments';
$txt['sp_function_Attachment_Recent_label'] = 'Recent Attachments';
$txt['sp_function_Calendar_label'] = 'Calendar';
$txt['sp_function_Calendar_Information_label'] = 'Calendar Information';
$txt['sp_function_Rss_Feed_label'] = 'RSS Feed';
$txt['sp_function_Theme_Select_label'] = 'Theme Selection';
$txt['sp_function_Staff_label'] = 'Staff List';
$txt['sp_function_Articles_label'] = 'Articles';
$txt['sp_function_Shoutbox_label'] = 'Shoutbox';
$txt['sp_function_Gallery_label'] = 'Gallery';
$txt['sp_function_Arcade_label'] = 'Arcade';
$txt['sp_function_Shop_label'] = 'Shop';
$txt['sp_function_Blog_label'] = 'Blog';
$txt['sp_function_Menu_label'] = 'Forum Menu';
$txt['sp_function_Bbc_label'] = 'Custom BBC';
$txt['sp_function_Html_label'] = 'Custom HTML';
$txt['sp_function_Php_label'] = 'Custom PHP';

// Block descriptions
$txt['sp_function_Latest_Member_desc'] = 'Displays the latest members and their registration dates.';
$txt['sp_function_User_Info_desc'] = 'Displays member information if the viewer is logged in, and a login box if the viewer is a guest.';
$txt['sp_function_Whos_Online_desc'] = 'Displays the who\'s online list.';
$txt['sp_function_Show_Poll_desc'] = 'Displays a poll with voting options if the viewer can vote, or the results if the viewer is unable to vote.';
$txt['sp_function_Board_Stats_desc'] = 'Displays some statistical information about the forum.';
$txt['sp_function_Quick_Search_desc'] = 'Displays a simple quick search form.';
$txt['sp_function_Top_Poster_desc'] = 'Displays the top posters with their post count and avatars.';
$txt['sp_function_Top_Stats_Member_desc'] = 'Displays various types of member stats.';
$txt['sp_function_Top_Boards_desc'] = 'Displays a list of boards according to their activity.';
$txt['sp_function_Top_Topics_desc'] = 'Displays a list topics according to their activity.';
$txt['sp_function_Recent_desc'] = 'Displays a list of the forums recent posts or topics.';
$txt['sp_function_Board_News_desc'] = 'Displays a list of posts from a selected board.';
$txt['sp_function_News_desc'] = 'Displays a random news line.';
$txt['sp_function_Attachment_Image_desc'] = 'Displays a list of recently attached images.';
$txt['sp_function_Attachment_Recent_desc'] = 'Displays a list of recent attachments.';
$txt['sp_function_Calendar_desc'] = 'Displays a full month calendar with all events.';
$txt['sp_function_Calendar_Information_desc'] = 'Displays calendar information such as birthdays, events and holidays.';
$txt['sp_function_Rss_Feed_desc'] = 'Displays an RSS Feed.';
$txt['sp_function_Theme_Select_desc'] = 'Displays a list of available themes and allows members to select one.';
$txt['sp_function_Staff_desc'] = 'Displays list of forum staff with their position and avatar.';
$txt['sp_function_Articles_desc'] = 'Displays recent or random articles.';
$txt['sp_function_Shoutbox_desc'] = 'Displays a shoutbox.';
$txt['sp_function_Gallery_desc'] = 'Displays a list of recent gallery items.';
$txt['sp_function_Arcade_desc'] = 'Displays various statistics from the arcade if an arcade mod is installed.';
$txt['sp_function_Shop_desc'] = 'Displays the richest members or various items from the shop if a shop mod is installed.';
$txt['sp_function_Blog_desc'] = 'Displays various information from the forum blog.';
$txt['sp_function_Menu_desc'] = 'Displays a vertical forum menu.';
$txt['sp_function_Bbc_desc'] = 'A custom block where BBC content can be added.';
$txt['sp_function_Html_desc'] = 'A custom block where HTML content can be added.';
$txt['sp_function_Php_desc'] = 'A custom block where PHP content can be added.';

// Block parameters
$txt['sp_param_Latest_Member_limit'] = 'Members to Show';
$txt['sp_param_Whos_Online_online_today'] = 'Users Online Today';
$txt['sp_param_Board_Stats_averages'] = 'Show Averages';
$txt['sp_param_Top_Poster_limit'] = 'Top Posters to Display';
$txt['sp_param_Top_Poster_type'] = 'Display';
$txt['sp_param_Top_Stats_Member_type'] = 'Type of the Top Member List';
$txt['sp_param_Top_Stats_Member_limit'] = 'Members to display';
$txt['sp_param_Top_Stats_Member_enable_label'] = 'Enable label for list';
$txt['sp_param_Top_Stats_Member_list_label'] = 'List label';
$txt['sp_param_Top_Stats_Member_sort_asc'] = 'Sort in reverse order';
$txt['sp_param_Top_Stats_Member_last_active_limit'] = 'Remove inactive Members from the List<br /><span class="smalltext">(Time in days)</span>';
$txt['sp_param_Recent_limit'] = 'Recent Posts or Topics to Display';
$txt['sp_param_Recent_type'] = 'Display';
$txt['sp_param_Recent_display'] = 'Display type';
$txt['sp_param_Recent_boards'] = 'Include boards';
$txt['sp_param_Top_Topics_type'] = 'Sorting Type';
$txt['sp_param_Top_Topics_limit'] = 'Topics to Display';
$txt['sp_param_Top_Boards_limit'] = 'Boards to Display';
$txt['sp_param_Show_Poll_topic'] = 'Topic ID';
$txt['sp_param_Show_Poll_type'] = 'Type';
$txt['sp_param_Board_News_board'] = 'Boards';
$txt['sp_param_Board_News_limit'] = 'Topics to Display';
$txt['sp_param_Board_News_start'] = 'Starting Post ID';
$txt['sp_param_Board_News_length'] = 'Maximum Characters';
$txt['sp_param_Board_News_avatar'] = 'Display Avatars';
$txt['sp_param_Board_News_per_page'] = 'Posts Per Page';
$txt['sp_param_Attachment_Image_limit'] = 'Images to Display';
$txt['sp_param_Attachment_Image_direction'] = 'List Direction';
$txt['sp_param_Attachment_Image_disablePoster'] = 'Remove Image Poster\'s Name';
$txt['sp_param_Attachment_Image_disableDownloads'] = 'Remove image Download';
$txt['sp_param_Attachment_Image_disableLink'] = 'Remove Image Download Link';
$txt['sp_param_Attachment_Recent_limit'] = 'Attachments to Display';
$txt['sp_param_Calendar_events'] = 'Show Events';
$txt['sp_param_Calendar_birthdays'] = 'Show Birthdays';
$txt['sp_param_Calendar_holidays'] = 'Show Holidays';
$txt['sp_param_Calendar_Information_events'] = 'Show Events';
$txt['sp_param_Calendar_Information_future'] = 'Number of days in future to show events';
$txt['sp_param_Calendar_Information_birthdays'] = 'Show Birthdays';
$txt['sp_param_Calendar_Information_holidays'] = 'Show Holidays';
$txt['sp_param_Rss_Feed_url'] = 'Feed URL';
$txt['sp_param_Rss_Feed_show_title'] = 'Show Title';
$txt['sp_param_Rss_Feed_show_content'] = 'Show Content';
$txt['sp_param_Rss_Feed_show_date'] = 'Show Date';
$txt['sp_param_Rss_Feed_strip_preserve'] = 'Tags to Preserve';
$txt['sp_param_Rss_Feed_count'] = 'Items to Show';
$txt['sp_param_Rss_Feed_limit'] = 'Character Limit';
$txt['sp_param_Staff_lmod'] = 'Disable Local Moderators';
$txt['sp_param_Articles_category'] = 'Category';
$txt['sp_param_Articles_limit'] = 'Articles to Display';
$txt['sp_param_Articles_type'] = 'Display Type';
$txt['sp_param_Articles_avatar'] = 'Author Avatar';
$txt['sp_param_Articles_length'] = 'Maximum Characters';
$txt['sp_param_Shoutbox_shoutbox'] = 'Shoutbox to Display';
$txt['sp_param_Gallery_limit'] = 'Items to Display';
$txt['sp_param_Gallery_type'] = 'Display';
$txt['sp_param_Gallery_direction'] = 'List Direction';
$txt['sp_param_Arcade_limit'] = 'Items to display';
$txt['sp_param_Arcade_type'] = 'Display';
$txt['sp_param_Shop_style'] = 'Display';
$txt['sp_param_Shop_limit'] = 'Items to display';
$txt['sp_param_Shop_type'] = 'Money type';
$txt['sp_param_Shop_sort'] = 'Item type';
$txt['sp_param_Blog_limit'] = 'Items to Display';
$txt['sp_param_Blog_type'] = 'Item Type';
$txt['sp_param_Blog_sort'] = 'Display';
$txt['sp_param_Html_content'] = 'Custom HTML';
$txt['sp_param_Bbc_content'] = 'Custom BBC';
$txt['sp_param_Php_content'] = 'Custom PHP';

// Parameter options
$txt['sp_param_Top_Poster_type_options'] = 'All Time|Today|This Week|This Month';
$txt['sp_param_Top_Stats_Member_type_options'] = 'Total Time Logged In|Posts|Good Karma|Bad Karma|Total  Karma|Thank-O-Matic Top Given|Thank-O-Matic Top Received|Automatic Karma Good|Automatic Karma Bad|Automatic Karma Total|Advanced Reputation System Best|Advanced Reputation System Worst';
$txt['sp_param_Recent_type_options'] = 'Posts|Topics';
$txt['sp_param_Recent_display_options'] = 'Compact|Full';
$txt['sp_param_Top_Topics_type_options'] = 'Replies|Views';
$txt['sp_param_Show_Poll_type_options'] = 'Normal|Recent|Random';
$txt['sp_param_Attachment_Image_direction_options'] = 'Vertical|Horizontal';
$txt['sp_param_Articles_type_options'] = 'Latest|Random';
$txt['sp_param_Gallery_type_options'] = 'Latest|Random';
$txt['sp_param_Gallery_direction_options'] = 'Vertical|Horizontal';
$txt['sp_param_Arcade_type_options'] = 'Most Played|Best Players|Longest Champ';
$txt['sp_param_Shop_style_options'] = 'Members|Items';
$txt['sp_param_Shop_type_options'] = 'Total|Pocket|Bank';
$txt['sp_param_Shop_sort_options'] = 'Recent|Random';
$txt['sp_param_Blog_type_options'] = 'Articles|Blogs';
$txt['sp_param_Blog_sort_options'] = 'Latest|Random';

// Articles Area
$txt['sp_admin_articles_title'] = 'Articles';
$txt['sp_admin_articles_list'] = 'Article List';
$txt['sp_admin_articles_add'] = 'Add Article';
$txt['sp_admin_articles_edit'] = 'Edit Article';
$txt['sp_admin_articles_remove'] = 'Remove Articles';
$txt['sp_admin_articles_preview'] = 'Preview';
$txt['sp_admin_articles_general'] = 'General Settings';

$txt['sp_admin_articles_desc'] = 'You can create and manage SimplePortal articles in this area.';
$txt['sp_articles_remove_confirm'] = 'Are you sure that you want to remove the selected articles?';
$txt['sp_admin_articles_delete_confirm'] = 'Are you sure you want to delete this article?';
$txt['sp_error_article_name_empty'] = 'Sorry, you left the article name empty.';
$txt['sp_error_article_namespace_empty'] = 'Sorry, you left the article ID empty.';
$txt['sp_error_article_namespace_duplicate'] = 'Sorry, that article ID is already in use.';
$txt['sp_error_article_namespace_invalid_chars'] = 'Sorry, there are invalid characters in the article ID. Article ID\'s can only contain letters, numbers and underscores.';
$txt['sp_error_article_namespace_numeric'] = 'Sorry, article ID\'s cannot be only numbers. Article ID\'s need letters, and or an underscore';

$txt['sp_admin_articles_col_title'] = 'Title';
$txt['sp_admin_articles_col_namespace'] = 'Article ID';
$txt['sp_admin_articles_col_category'] = 'Category';
$txt['sp_admin_articles_col_author'] = 'Author';
$txt['sp_admin_articles_col_body'] = 'Body';
$txt['sp_admin_articles_col_type'] = 'Type';
$txt['sp_admin_articles_col_status'] = 'Active';
$txt['sp_admin_articles_col_date'] = 'Date';
$txt['sp_admin_articles_col_actions'] = 'Actions';
$txt['sp_admin_articles_col_permissions'] = 'Permissions';
$txt['sp_admin_articles_activate'] = 'Activate';
$txt['sp_admin_articles_deactivate'] = 'Deactivate';

$txt['sp_articles_default_title'] = 'Untitled Article';

$txt['sp_articles_type_bbc'] = 'BBC';
$txt['sp_articles_type_html'] = 'HTML';
$txt['sp_articles_type_php'] = 'PHP';

// Categories Area
$txt['sp_admin_categories_title'] = 'Categories';
$txt['sp_admin_categories_list'] = 'Category List';
$txt['sp_admin_categories_add'] = 'Add Category';
$txt['sp_admin_categories_edit'] = 'Edit Category';
$txt['sp_admin_categories_remove'] = 'Remove Categories';

$txt['sp_admin_categories_desc'] = 'You can create and manage SimplePortal categories in this area.';
$txt['sp_categories_remove_confirm'] = 'Are you sure that you want to remove the selected categories?';
$txt['sp_admin_categories_delete_confirm'] = 'Are you sure you want to delete this category?';
$txt['sp_error_category_name_empty'] = 'Sorry, you left the category name empty.';
$txt['sp_error_category_namespace_empty'] = 'Sorry, you left the category ID empty.';
$txt['sp_error_category_namespace_duplicate'] = 'Sorry, that category ID is already in use.';
$txt['sp_error_category_namespace_invalid_chars'] = 'Sorry, there are invalid characters in the category ID. Category ID\'s can only contain letters, numbers and underscores.';
$txt['sp_error_category_namespace_numeric'] = 'Sorry, category ID\'s cannot be only numbers. Category ID\'s need letters, and or an underscore';

$txt['sp_admin_categories_col_name'] = 'Name';
$txt['sp_admin_categories_col_namespace'] = 'Category ID';
$txt['sp_admin_categories_col_description'] = 'Description';
$txt['sp_admin_categories_col_articles'] = 'Articles';
$txt['sp_admin_categories_col_status'] = 'Active';
$txt['sp_admin_categories_col_actions'] = 'Actions';
$txt['sp_admin_categories_col_permissions'] = 'Permissions';
$txt['sp_admin_categories_activate'] = 'Activate';
$txt['sp_admin_categories_deactivate'] = 'Deactivate';

$txt['sp_categories_default_name'] = 'Unnamed Category';

// Pages Area
$txt['sp_admin_pages_title'] = 'Pages';
$txt['sp_admin_pages_list'] = 'Page List';
$txt['sp_admin_pages_add'] = 'Add Page';
$txt['sp_admin_pages_edit'] = 'Edit Page';
$txt['sp_admin_pages_remove'] = 'Remove Pages';
$txt['sp_admin_pages_preview'] = 'Preview';
$txt['sp_admin_pages_general'] = 'General Settings';
$txt['sp_admin_pages_style'] = 'Style Options';

$txt['sp_admin_pages_desc'] = 'You can create and manage SimplePortal pages in this area.';
$txt['sp_pages_remove_confirm'] = 'Are you sure that you want to remove the selected pages?';
$txt['sp_admin_pages_delete_confirm'] = 'Are you sure you want to delete this page?';
$txt['sp_error_page_name_empty'] = 'Sorry, you left the page name empty.';
$txt['sp_error_page_namespace_empty'] = 'Sorry, you left the page ID empty.';
$txt['sp_error_page_namespace_duplicate'] = 'Sorry, that page ID is already in use.';
$txt['sp_error_page_namespace_invalid_chars'] = 'Sorry, there are invalid characters in the page ID. Page ID\'s can only contain letters, numbers and underscores.';
$txt['sp_error_page_namespace_numeric'] = 'Sorry, page ID\'s cannot be only numbers. Page ID\'s need letters, and or an underscore';

$txt['sp_admin_pages_col_title'] = 'Title';
$txt['sp_admin_pages_col_namespace'] = 'Page ID';
$txt['sp_admin_pages_col_body'] = 'Body';
$txt['sp_admin_pages_col_type'] = 'Type';
$txt['sp_admin_pages_col_views'] = 'Views';
$txt['sp_admin_pages_col_status'] = 'Active';
$txt['sp_admin_pages_col_actions'] = 'Actions';
$txt['sp_admin_pages_col_permissions'] = 'Permissions';
$txt['sp_admin_pages_col_blocks'] = 'Blocks';
$txt['sp_admin_pages_activate'] = 'Activate';
$txt['sp_admin_pages_deactivate'] = 'Deactivate';

$txt['sp_pages_default_title'] = 'Untitled Page';

$txt['sp_pages_type_bbc'] = 'BBC';
$txt['sp_pages_type_html'] = 'HTML';
$txt['sp_pages_type_php'] = 'PHP';

// Shoutboxes Area
$txt['sp_admin_shoutbox_title'] = 'Shoutbox';
$txt['sp_admin_shoutbox_list'] = 'Shoutbox List';
$txt['sp_admin_shoutbox_add'] = 'Add Shoutbox';
$txt['sp_admin_shoutbox_edit'] = 'Edit Shoutbox';
$txt['sp_admin_shoutbox_prune'] = 'Prune Shoutbox';
$txt['sp_admin_shoutbox_remove'] = 'Remove Shoutboxes';

$txt['sp_admin_shoutbox_desc'] = 'You can create and manage SimplePortal shoutboxes in this area.';
$txt['sp_shoutbox_remove_confirm'] = 'Are you sure that you want to remove the selected shoutboxes?';
$txt['sp_admin_shoutbox_delete_confirm'] = 'Are you sure you want to delete this shoutbox?';
$txt['sp_error_no_shoutbox'] = 'There are no shoutboxes available.';
$txt['sp_error_shoutbox_name_duplicate'] = 'Sorry, there is already a shoutbox with that name.';

$txt['sp_admin_shoutbox_col_name'] = 'Name';
$txt['sp_admin_shoutbox_col_shouts'] = 'Shouts';
$txt['sp_admin_shoutbox_col_caching'] = 'Caching';
$txt['sp_admin_shoutbox_col_status'] = 'Status';
$txt['sp_admin_shoutbox_col_actions'] = 'Actions';
$txt['sp_admin_shoutbox_col_permissions'] = 'Permissions';
$txt['sp_admin_shoutbox_col_moderators'] = 'Moderators';
$txt['sp_admin_shoutbox_col_reverse'] = 'Reverse Order';
$txt['sp_admin_shoutbox_activate'] = 'Activate';
$txt['sp_admin_shoutbox_deactivate'] = 'Deactivate';

$txt['sp_shoutbox_default_name'] = 'Untitled Shoutbox';

$txt['sp_admin_shoutbox_col_warning'] = 'Warning';
$txt['sp_admin_shoutbox_col_bbc'] = 'Allowed BBC';
$txt['sp_admin_shoutbox_col_height'] = 'Height<span class="smalltext">(pixels)</span>';
$txt['sp_admin_shoutbox_col_num_show'] = 'Shouts to display';
$txt['sp_admin_shoutbox_col_num_max'] = 'Maximum shouts';
$txt['sp_admin_shoutbox_col_refresh'] = 'Auto refresh<span class="smalltext">(seconds)</span>';

$txt['sp_admin_shoutbox_opt_all'] = 'Delete all shouts';
$txt['sp_admin_shoutbox_opt_days'] = 'Delete shouts older than a number of days';
$txt['sp_admin_shoutbox_opt_member'] = 'Delete shouts posted by a member';

$txt['sp_admin_shoutbox_block_redirect_message'] = '<p>Congratulations, you have created a new shoutbox!</p>
<p>Although it won\'t be displayed anywhere within your forum yet. You will need to create a shoutbox block before your users can use the shoutbox.</p>
<p>Click <a href="%s">here</a> to create a new shoutbox block for this shoutbox.</p>
<p>Click <a href="%s">here</a> to continue to your list of shoutboxes.</p>';

// Profiles Area
$txt['sp_admin_profiles_title'] = 'Profiles';
$txt['sp_admin_profiles_add'] = 'Add Profile';
$txt['sp_admin_profiles_edit'] = 'Edit Profile';
$txt['sp_admin_profiles_remove'] = 'Remove Profiles';

$txt['sp_admin_permission_profiles_list'] = 'Permission Profiles List';
$txt['sp_admin_permission_profiles_add'] = 'Add Permission Profile';

$txt['sp_admin_profiles_desc'] = 'You can create and manage SimplePortal profiles in this area.';
$txt['sp_admin_profiles_delete_confirm'] = 'Are you sure you want to delete this profile?';
$txt['sp_profiles_remove_confirm'] = 'Are you sure that you want to remove the selected profiles?';
$txt['sp_error_profile_name_empty'] = 'Sorry, you left the profile name empty.';
$txt['sp_error_no_profiles'] = 'There are no profiles available.';

$txt['sp_admin_profiles_col_name'] = 'Name';
$txt['sp_admin_profiles_col_articles'] = 'Articles';
$txt['sp_admin_profiles_col_blocks'] = 'Blocks';
$txt['sp_admin_profiles_col_categories'] = 'Categories';
$txt['sp_admin_profiles_col_pages'] = 'Pages';
$txt['sp_admin_profiles_col_shoutboxes'] = 'Shoutboxes';
$txt['sp_admin_profiles_col_permissions'] = 'Permissions';
$txt['sp_admin_profiles_col_actions'] = 'Actions';

$txt['sp_profiles_default_name'] = 'Unnamed Profile';

$txt['sp_admin_profiles_permissions_membergroup'] = 'Membergroup';
$txt['sp_admin_profiles_permissions_allowed_short'] = 'A';
$txt['sp_admin_profiles_permissions_disallowed_short'] = 'X';
$txt['sp_admin_profiles_permissions_denied_short'] = 'D';
$txt['sp_admin_profiles_permissions_allowed'] = 'Allowed';
$txt['sp_admin_profiles_permissions_disallowed'] = 'Disallowed';
$txt['sp_admin_profiles_permissions_denied'] = 'Denied';

$txt['sp_admin_profiles_guests'] = 'Guests';
$txt['sp_admin_profiles_members'] = 'Members';
$txt['sp_admin_profiles_everyone'] = 'Everyone';

// General titles
$txt['sp-adminColumnAction'] = 'Action';
$txt['sp-adminColumnName'] = 'Name';

// Block list titles
$txt['sp-adminColumnType'] = 'Type';
$txt['sp-adminColumnMove'] = 'Move';

// Miscellaneous strings
$txt['sp-positionLeft'] = 'Left';
$txt['sp-positionTop'] = 'Top';
$txt['sp-positionBottom'] = 'Bottom';
$txt['sp-positionRight'] = 'Right';
$txt['sp-positionHeader'] = 'Header';
$txt['sp-positionFooter'] = 'Footer';
$txt['sp-placementBefore'] = 'Before';
$txt['sp-placementAfter'] = 'After';
$txt['sp-placementUnchanged'] = 'Unchanged';
$txt['sp-stateYes'] = 'Yes';
$txt['sp-stateNo'] = 'No';

// Information area
$txt['sp-info_title'] = 'Information';
$txt['sp-info_desc'] = 'Some useful system, and SimplePortal information.';
$txt['sp-info_live'] = 'Live from SimplePortal...';
$txt['sp-info_no_live'] = 'Sorry! At this time you are unable to connect to simpleportal.net\'s latest news file.';
$txt['sp-info_general'] = 'General Information';
$txt['sp-info_versions'] = 'Version Information';
$txt['sp-info_your_version'] = 'Your Version';
$txt['sp-info_current_version'] = 'Current Version';
$txt['sp-info_managers'] = 'Managers';
$txt['sp-info_intro'] = 'The SimplePortal Team wants to thank everyone who helped make SimplePortal what it is today, and the Elkarte Team for the great forum software. It wouldn\'t have been possible without you, our users, and Elkarte.';
$txt['sp-info_team'] = 'The Team';
$txt['sp-info_special'] = 'Special Thanks';
$txt['sp-info_and'] = 'and';
$txt['sp-info_anyone'] = 'For anyone we may have missed, thank you!';
$txt['sp-info_groups_pm'] = 'Project Managers';
$txt['sp-info_groups_dev'] = 'Developers';
$txt['sp-info_groups_support'] = 'Support Specialists';
$txt['sp-info_groups_customize'] = 'Customizers';
$txt['sp-info_groups_language'] = 'Language Managers';
$txt['sp-info_groups_marketing'] = 'Marketing';
$txt['sp-info_groups_beta'] = 'Beta Testers';
$txt['sp-info_groups_translators'] = 'Language Translators';
$txt['sp-info_translators_message'] = 'Thank you for your efforts in the internationalization of SimplePortal.';
$txt['sp-info_groups_founder'] = 'Founding Father of SimplePortal';
$txt['sp-info_groups_orignal_pm'] = 'Original Project Managers';
$txt['sp-info_fam_fam'] = 'Pretty Icons';
$txt['sp-info_fam_fam_message'] = 'Mark James for his <a href="http://www.famfamfam.com/lab/icons/silk/">Fam Fam Fam Silk Icons</a>.';
$txt['sp-info_contribute'] = 'Did you find SimplePortal useful? <a href="%s" target="_blank">Contribute to the project!</a>';

// Permissions
$txt['permissiongroup_sp'] = 'SimplePortal';
$txt['permissiongroup_simple_sp'] = 'SimplePortal';
$txt['permissionname_sp_admin'] = 'Administrate portal';
$txt['permissionhelp_sp_admin'] = 'This permission allows users to administrate SimplePortal.';
$txt['permissionname_sp_manage_blocks'] = 'Manage portal blocks';
$txt['permissionhelp_sp_manage_blocks'] = 'This permission allows users to manage SimplePortal blocks.';
$txt['permissionname_sp_manage_articles'] = 'Manage portal articles';
$txt['permissionhelp_sp_manage_articles'] = 'This permission allows users to manage SimplePortal articles.';
$txt['permissionname_sp_manage_pages'] = 'Manage portal pages';
$txt['permissionhelp_sp_manage_pages'] = 'This permission allows users to manage SimplePortal pages.';
$txt['permissionname_sp_manage_shoutbox'] = 'Manage portal shoutboxes';
$txt['permissionhelp_sp_manage_shoutbox'] = 'This permission allows users to manage SimplePortal shoutboxes.';
$txt['permissionname_sp_manage_profiles'] = 'Manage portal profiles';
$txt['permissionhelp_sp_manage_profiles'] = 'This permission allows users to manage SimplePortal profiles.';
$txt['permissionname_sp_manage_settings'] = 'Manage portal settings';
$txt['permissionhelp_sp_manage_settings'] = 'This permission allows users to manage SimplePortal settings.';

// Compatibility strings
$txt['sp-adminCatHelp'] = 'Here you can manage and configure SimplePortal.';
$txt['sp-adminCatDesc'] = 'Here you can manage and configure SimplePortal.';

// Errors
$txt['sp_form_errors_detected'] = 'The following error or errors occurred while saving or editing:';